'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UserSchema extends Schema {
  up () {
    this.create('usuarios', (table) => {
      table.increments('UsuarioId').notNullable()
      table.string('UsuarioNombreCompleto', 100).notNullable()
      table.string('UsuarioCorreo', 100).notNullable().unique()
      table.string('UsuarioPassword', 100).notNullable()
      table.string('UsuarioToken', 150)
      table.enu('UsuarioEstatus', ['activo', 'inactivo']).notNullable().defaultTo('activo')
      table.enu('UsuarioAutenticacionTipo', ['facebook', 'twitter', 'email']).notNullable();
      table.timestamps()
    })
  }

  down () {
    this.drop('usuarios')
  }
}

module.exports = UserSchema
