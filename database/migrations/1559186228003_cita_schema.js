'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CitaSchema extends Schema {
  up () {
    this.create('citas', (table) => {
      table.increments('CitaId').notNullable().unique()
      table.text('CitaLibro').notNullable()
      table.integer('CitaCapitulo').notNullable()
      table.integer('CitaVersiculo').notNullable()
      table.enu('CitaEstatus', ['activo', 'inactivo']).notNullable().defaultTo('activo')
      table.timestamps()
    })
  }

  down () {
    this.drop('citas')
  }
}

module.exports = CitaSchema
