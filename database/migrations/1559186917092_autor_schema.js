'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AutorSchema extends Schema {
  up () {
    this.create('autores', (table) => {
      table.increments('AutorId').notNullable().unique()
      table.string('AutorNombre', 100).notNullable();
      table.enu('AutorEstatus', ['activo', 'inactivo']).notNullable().defaultTo('activo')
      table.timestamps()
    })
  }

  down () {
    this.drop('autores')
  }
}

module.exports = AutorSchema
