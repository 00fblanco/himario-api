'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PlaylistSchema extends Schema {
  up () {
    this.create('playlists', (table) => {
      table.increments('PlaylistId')
      table.string('PlaylistTitulo', 100).notNullable()
      table.text('PlaylistDescripcion')
      table.enu('PlaylistEstatus', ['activo', 'inactivo']).notNullable().defaultTo('activo')
      table.integer('UsuarioId').unsigned().notNullable().references('UsuarioId').inTable('Usuarios');

      table.timestamps()
    })
  }

  down () {
    this.drop('playlists')
  }
}

module.exports = PlaylistSchema
