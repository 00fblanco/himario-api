'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class HimnoCitaSchema extends Schema {
  up () {
    this.create('himno_cita', (table) => {
      table.integer('HimnoId').unsigned().notNullable().references('HimnoId').inTable('Himnos');
      table.integer('CitaId').unsigned().notNullable().references('CitaId').inTable('Citas');
      table.integer('HimnoCitaOrden').notNullable()
      table.string('HimnoCitaPrefijo', 1).notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('himno_cita')
  }
}

module.exports = HimnoCitaSchema
