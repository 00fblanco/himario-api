'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class HimnosSchema extends Schema {
  up () {
    this.create('himnos', (table) => {
      table.increments('HimnoId');
      table.integer('HimnoNumero').notNullable().unique();
      table.text('HimnoTitulo', 100);
      table.text('HimnoContenido')
      table.text('HimnoPartituraPath')
      table.text('HimnoTextoBiblico')
      table.text('HimnoDatosComplementarios')
      table.enu('HimnoEstatus', ['activo', 'inactivo']).notNullable().defaultTo('activo')
      table.timestamps()
    })
  }

  down () {
    this.drop('himnos')
  }
}

module.exports = HimnosSchema
