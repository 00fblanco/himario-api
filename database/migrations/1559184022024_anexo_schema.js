'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AnexoSchema extends Schema {
  up () {
    this.create('anexos', (table) => {
      table.increments('AnexoId')
      table.integer('AnexoNumero').notNullable()
      table.text('AnexoTitulo').notNullable()
      table.text('AnexoCita')
      table.text('AnexoContenido').notNullable()
      table.enu('AnexoEstatus', ['activo', 'inactivo']).notNullable().defaultTo('activo')
      table.timestamps()
    })
  }

  down () {
    this.drop('anexos')
  }
}

module.exports = AnexoSchema
