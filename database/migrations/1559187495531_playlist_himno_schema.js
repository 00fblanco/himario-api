'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PlaylistHimnoSchema extends Schema {
  up () {
    this.create('playlist_himno', (table) => {
      table.integer('PlaylistId ').unsigned().notNullable().references('PlaylistId ').inTable('Playlists');
      table.integer('HimnoId ').unsigned().notNullable().references('HimnoId ').inTable('Himnos');
      table.integer('PlaylistHimnoOrden').notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('playlist_himno')
  }
}

module.exports = PlaylistHimnoSchema
