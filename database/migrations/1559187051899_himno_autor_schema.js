'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class HimnoAutorSchema extends Schema {
  up () {
    this.create('himno_autor', (table) => {
      table.integer('HimnoId').unsigned().notNullable().references('HimnoId').inTable('Himnos');
      table.integer('AutorId').unsigned().notNullable().references('AutorId').inTable('Autores');
      table.enu('HimnoAutorTipo', ['L','M', 'A', 'O'])
      table.text('HimnoAutorComplemento')
      table.string('HimnoAutorAnio', 10)
      table.timestamps()
    })
  }

  down () {
    this.drop('himno_autor')
  }
}

module.exports = HimnoAutorSchema
