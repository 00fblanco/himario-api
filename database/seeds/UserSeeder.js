'use strict'

const Factory = use('Factory')

class UserSeeder {
  async run () {
    const usersArray = await Factory
      .model('App/Models/Usuario')
      .createMany(5)
  }
}

module.exports = UserSeeder
