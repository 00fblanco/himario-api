'use strict'
const Factory = use('Factory')

Factory.blueprint('App/Models/Usuario', async (faker) => {
   return {
      UsuarioNombreCompleto: faker.username(),
      UsuarioCorreo: faker.email(),
      UsuarioPassword: '123456',
      UsuarioAutenticacionTipo: 'email',
   }
})

