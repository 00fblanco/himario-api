'use strict'
// Libraries
const Hash = use('Hash');
const Database = use('Database');
const { validate } = use("Validator");
const Mail = use('Mail');

// Models
const Usuario = use('App/Models/Usuario');



class UsuarioController {
    async redirectToProvider ({ ally, params }) {
        console.log(params.provider)
        await ally.driver(params.provider).redirect()
    }

    async handleProviderCallback ({params, ally, auth, response}) {
        const provider = params.provider
        try {
            console.log('comming')
            const fbUser = await ally.driver(provider).getUser();
            console.log(fbUser)
            const userDetails = {
                email: fbUser.getEmail(),
                token: fbUser.getAccessToken(),
                login_source: 'facebook'
            }

            const user = await User.findOrCreate({email: fbUser.getEmail()}, userDetails)

            await auth.login(user);

            return response.status(200).json({user})
        } catch (error) {
            return response.status(203).json({msg: 'Error al autenticar con facebook. ' + error })
        }
    }

    async login({ request, response, auth }){
        const { UsuarioCorreo, UsuarioPassword } = request.post();

        const usuario = await Usuario.findBy('UsuarioCorreo', UsuarioCorreo);

        if(!usuario)
            return response.status(401).json({ msg: 'No se encontro el correo del usuario.'});
        

        const verifyPassword = await Hash.verify(UsuarioPassword,usuario.UsuarioPassword);

        if(!verifyPassword){
            return response.status(401).json({ msg: 'La contraseña es incorrecta' });
        }

        const token = await auth.generate(usuario);

        return response.status(200).json({ token, usuario })
    }
    
    async signup({ request, response, auth }){
        const userData = request.only(["UsuarioNombreCompleto", "UsuarioCorreo", "UsuarioPassword"]);

        const validationUser = await validate(userData, Usuario.storeRules, Usuario.errorMessages);
        

        if(validationUser.fails()){
            return response.status(401).json({
                messages: validationUser.messages()
            });
        }

        const usuario = await Usuario.create({...userData, UsuarioAutenticacionTipo: 'email'});

        await Mail.raw('Correo de prueba desde el sistema de Himnario alv prrro', (message) => {
            message.from('fblanco@gmail.com')
            message.to(usuario.UsuarioCorreo)
        })

        const token = await auth.generate(usuario);

        return response.status(201).json({token, usuario });
    }

    async update({ request, response, auth }){
        const dataForUpdate = request.only(['UsuarioNombreCompleto', 'UsuarioPassword']);

        const usuario = await auth.user;
        if(!usuario.UsuarioAutenticacionTipo == 'email'){
            return response.status(201).json({msg: 'Solo los usuarios registros por correo pueden actualizar sus datos'});
        }

        const validationUpdate = await validate(dataForUpdate, { UsuarioPassword: 'required|min:6|max:20', UsuarioNombreCompleto: 'required|min:6|max:50|alpha' }, Usuario.errorMessages);
        
        if(validationUpdate.fails()){
            return response.status(401).json({
                messages: validationUpdate.messages()
            });
        }
        
        const affectedRows = await Database.table('usuarios')
            .where('UsuarioCorreo', usuario.UsuarioCorreo)
            .update({...dataForUpdate, UsuarioPassword: await Hash.make(dataForUpdate.UsuarioPassword)});
        
        return response.status(200).json({ msg: 'Datos actualizados correctamente.'});
    }
    
}

module.exports = UsuarioController
