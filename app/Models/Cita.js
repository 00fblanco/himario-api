'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Cita extends Model {

    static get table(){
        return 'citas'
    }

    static get primaryKey(){
        return 'CitaId'
    }
}

module.exports = Cita
