'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class PlaylistHimno extends Model {

    static get table(){
        return 'playlist_himno'
    }

    
}

module.exports = PlaylistHimno
