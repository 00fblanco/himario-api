'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Playlist extends Model {

    static get table(){
        return 'playlists'
    }

    static get primaryKey(){
        return 'PlaylistId'
    }
}

module.exports = Playlist
