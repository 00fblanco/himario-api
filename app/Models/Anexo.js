'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Anexo extends Model {

    static get table(){
        return 'anexos';
    }

    static get primaryKey () {
        return 'UsuarioId'
    }
}

module.exports = Anexo
