'use strict'

const Model = use('Model')
const Hash = use('Hash')

class Usuario extends Model {
  static boot () {
    super.boot()

    this.addHook('beforeSave', async (userInstance) => {
      if (userInstance.dirty.UsuarioPassword) {
        userInstance.UsuarioPassword = await Hash.make(userInstance.UsuarioPassword)
      }
    })
  }

  static get primaryKey () {
    return 'UsuarioId'
  }
  static get table(){
    return 'usuarios'
  }
  static get hidden(){
    return ['password']
  }

  static get storeRules(){
    return {
      UsuarioCorreo: 'required|email|unique:usuarios,UsuarioCorreo',
      UsuarioPassword: 'required|min:6|max:20',
      UsuarioNombreCompleto: 'required|min:6|max:50|string'
    }
  }

  static get errorMessages(){
    return {
        'UsuarioCorreo': '{{field}} es requerido',
        'email': '{{field}} debe tener formato de un correo, ejemplo: 00fblanco@dev.com',
        'min': '{{field}} debe tener como minimo {{argument.0}} caracteres',
        'max': '{{field}} debe tener como maximo {{argument.1}} caracteres',
        'integer': "{{field}} debe ser numerico entero",
        'unique': "{{field}} debe ser unico, ya existe un registro",
        'range': "{{field}} debe estar entre {{argument.0}} y {{argument.1}}",
        'alpha_numeric': "{{field}} debe ser alfa numérico solamente",
        'alpha': "{{field}} debe contener solo letras."
    }
  }
}

module.exports = Usuario
