'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Autor extends Model {

    static get table(){
        return 'Autores'
    }

    static get primaryKey(){
        return 'AutorId'
    }

}

module.exports = Autor
