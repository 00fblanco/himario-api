'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Himno extends Model {
    static get table(){
        return 'himnos';
    }

    static get primaryKey () {
        return 'HimnoId'
    }
}

module.exports = Himno
