'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class HimnoAutor extends Model {

    static get table(){
        return 'himno_autor'
    }
}

module.exports = HimnoAutor
