'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URLs and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.0/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')



Route.get('auth/:provider', 'UserController.redirectToProvider')

Route.get('authenticated/:provider', 'UserController.handleProviderCallback')




Route.group(() => { 
    Route.get('/', 'HimnoController.protectedRoute').middleware(['auth'])
}).prefix('/api/himno')


Route.group(() => { 
    Route.post('/login', 'UsuarioController.login')
    Route.post('/signup', 'UsuarioController.signup')
    Route.put('/update', 'UsuarioController.update').middleware(['auth'])
}).prefix('/api/usuarios');


